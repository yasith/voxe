package net.tuxv.voxe;

/**
 * Created by yasith on 21/02/16.
 */
public class DateUtils {
    private static final String[] MONTHS = {"Spring", "Summer", "Fall", "Winter"};
    private static final int minsInHour = 60;
    private static final int hoursInDay = 24;
    private static final int daysInMonth = 30;
    private static final int monthsInYear = 4;

    private static final int startYear = 1;
    private static final int startMonth = 0;
    private static final int startDay = 1;

    public static String getDateAsString(long time) {
        int mins = (int) time % minsInHour;

        time /= minsInHour; // time in hours
        int hours = (int) time % hoursInDay;

        time /= hoursInDay; // time in days
        int days = (int) time % daysInMonth;

        time /= daysInMonth; // time in months
        int months = (int) time % monthsInYear;

        time /= monthsInYear; // time in years
        int years = (int) time;

        String date = "Year " + (startYear + years) + " " +
                      MONTHS[(startMonth + months) % monthsInYear] + " " +
                      (startDay + days) + " " +
                      hours + ":" + mins;

        return date;
    }

    public static int getHour(long time) {
        return ((int) time / minsInHour) % hoursInDay;
    }

    /**
     * Ambient light level is dependent on the location of the sun.
     * We use a sine wave to emulate the sun.
     *
     * @param time world time given by minute, starting from Year 1, Spring 1, 00:00
     * @return float indicating the sun level for ambient light.
     *         0.2 for lowest (no sun), 1 for highest.
     */
    public static float getAmbientLight(long time) {
        int hour = getHour(time);

        // TODO: Change sunrise/sunset depending on season
        float sunRise = 4.0f;
        float sunSet = 20.0f;

        if(hour < sunRise || hour > sunSet) {
            return 0.2f;
        }

        float percentile = (hour - sunRise) / (sunSet - sunRise);
        float level = (float) Math.sin(percentile * Math.PI);

        return level + 0.2f;
    }

    public static String getGameTimeAsString() {
        return getDateAsString(GameState.getInstance().getGameTime());
    }
}
