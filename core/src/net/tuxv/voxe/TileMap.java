package net.tuxv.voxe;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.components.MainItemComponent;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

/**
 * Created by yasith on 24/02/16.
 */
public class TileMap {
    public static final int TILE_SIZE = 8; // in world units
    public static final String GROUND = "ground";
    private final Tile[][] map;

    public TileMap(SceneLoader sceneLoader) {
        map = new Tile[100][100];
        initMap(sceneLoader.getEngine());
    }

    private void initMap(Engine engine) {
        ImmutableArray<Entity> entities = engine.getEntities();

        for(Entity entity : entities) {
            MainItemComponent mainItemComponent = ComponentRetriever.get(entity, MainItemComponent.class);
            if(mainItemComponent.tags.contains(GROUND)) {
                TransformComponent transformComponent = ComponentRetriever.get(entity, TransformComponent.class);

                int col = (int) (transformComponent.x / TILE_SIZE);
                int row = (int) (transformComponent.y / TILE_SIZE);

                map[col][row] = new Tile(row, col, entity);

                // TODO: Implement getTileByTag
                // https://github.com/azakhary/thm/blob/master/core%2Fsrc%2Fcom%2Funderwater%2Fthm%2FTiledWorld.java
            }
        }
    }

    public Tile getTileAt(int x, int y) {
        return map[x][y];
    }

}
