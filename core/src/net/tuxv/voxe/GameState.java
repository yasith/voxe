package net.tuxv.voxe;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yasith on 21/02/16.
 */
public class GameState {
    private static GameState instance;

    @Getter
    @Setter
    private long gameTime;

    private GameState() {
        gameTime = 0;
    }

    public static GameState getInstance() {
        if(instance == null) {
            instance = new GameState();
        }

        return instance;
    }

    public long incrementTime(long delta) {
        gameTime += delta;
        return gameTime;
    }
}
