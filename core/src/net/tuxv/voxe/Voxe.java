package net.tuxv.voxe;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

public class Voxe extends ApplicationAdapter {

    public static final String MAIN_SCENE = "MainScene";
    public static final int WORLD_WIDTH = 8 * 20; // 20 tiles, a tile is 8 WUs
    public static final int WORLD_HEIGHT = 8 * 10; // 10 tiles, a tile is 8 WUs

    private OrthographicCamera camera;
    private SceneLoader sceneLoader;
    private Viewport viewport;
    private Hero hero;

    Box2DDebugRenderer debugRenderer;
    private TileMap tileMap;
    private TileMapController tileMapController;
    private GameState gameState;
    private float timer;
    private long timeAccelerateStep = 10;

    @Override
	public void create () {
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT);
        camera = (OrthographicCamera) viewport.getCamera();
		sceneLoader = new SceneLoader();
		sceneLoader.loadScene(MAIN_SCENE, viewport);

        ItemWrapper root = new ItemWrapper(sceneLoader.getRoot());

        tileMap = new TileMap(sceneLoader);
        tileMapController = new TileMapController(sceneLoader, tileMap);

        hero = new Hero(sceneLoader, tileMapController);
        root.getChild("hero").addScript(hero);

        Hero.Torch torch = new Hero.Torch(hero);
        root.getChild("torch").addScript(torch);

        debugRenderer = new Box2DDebugRenderer();

        gameState = GameState.getInstance();
        gameState.setGameTime(0);
	}

	@Override
	public void render () {
        float deltaTime = Gdx.graphics.getDeltaTime();
        timer += deltaTime;
        if(timer > 10)  {
            gameState.incrementTime(10);
            timer = 0;
        }
        float lightLevel = DateUtils.getAmbientLight(gameState.getGameTime());
        sceneLoader.rayHandler.setAmbientLight(lightLevel);
        System.out.println("GameTime: " + DateUtils.getGameTimeAsString());
        System.out.println("Setting light level to: " + lightLevel);
        System.out.println("Actual light level: " + sceneLoader.rayHandler.toString());

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		sceneLoader.getEngine().update(deltaTime);

        camera.position.set(hero.getX(), hero.getY(), 0);

        processDebugDetails();
	}

    private void processDebugDetails() {
        debugRenderer.render(sceneLoader.world, camera.combined);

        // Accelerate Time
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            gameState.incrementTime(timeAccelerateStep);
        }

        // Modify time acceleration
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && Gdx.input.isKeyPressed(Input.Keys.UP)) {
            timeAccelerateStep += 10;
        } else if(Gdx.input.isKeyPressed(Input.Keys.SPACE) && Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            timeAccelerateStep -= 10;
        }
    }
}
