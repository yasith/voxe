package net.tuxv.voxe;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.components.TextureRegionComponent;
import com.uwsoft.editor.renderer.components.TintComponent;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;
import lombok.AllArgsConstructor;
import lombok.Setter;
import sun.nio.cs.ext.COMPOUND_TEXT;

/**
 * Created by yasith on 24/02/16.
 */
@AllArgsConstructor
public class TileMapController {

    public static final String GROUND_TILLED = "groundtilled";
    SceneLoader sceneLoader;
    @Setter TileMap map;

    public boolean useHoe(Vector2 target) {
        Entity tileEntity = getTileEntity(target);

        TextureRegionComponent textureRegionComponent = ComponentRetriever.get(tileEntity, TextureRegionComponent.class);
        textureRegionComponent.region = sceneLoader.getRm().getTextureRegion(GROUND_TILLED);

        return true;
    }

    public void highlightTarget(Vector2 target) {
        Entity tileEntity = getTileEntity(target);

        TintComponent tintComponent = ComponentRetriever.get(tileEntity, TintComponent.class);
        System.out.print(tintComponent.color.toString());
        tintComponent.color = new Color(100, 0, 0, 50);
    }

    public void removeHighlight(Vector2 target) {
        Entity tileEntity = getTileEntity(target);

        TintComponent tintComponent = ComponentRetriever.get(tileEntity, TintComponent.class);
        tintComponent.color = new Color(255, 255, 255, 255);
    }

    private Tile getTile(Vector2 target) {
        int mapX = (int) (target.x / TileMap.TILE_SIZE);
        int mapY = (int) (target.y / TileMap.TILE_SIZE);

        return map.getTileAt(mapX, mapY);
    }

    private Entity getTileEntity(Vector2 target) {
        return getTile(target).getEntity();
    }

}
