package net.tuxv.voxe;

import com.badlogic.ashley.core.Entity;
import lombok.Data;

/**
 * Created by yasith on 24/02/16.
 */
@Data
class Tile {

    private final int row;
    private final int col;

    private final Entity entity;

    public Tile(int row, int col, Entity entity) {
        this.row = row;
        this.col = col;
        this.entity = entity;
    }
}
