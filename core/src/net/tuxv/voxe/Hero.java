package net.tuxv.voxe;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.components.DimensionsComponent;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

/**
 * Created by yasith on 22/02/16.
 */
public class Hero implements IScript {

    private final SceneLoader sceneLoader;
    private Entity entity;
    private TransformComponent transformComponent;

    private float velocity = 50f;
    private DimensionsComponent dimensionsComponent;
    private boolean blocked;

    private TileMapController tileMapController;

    private Vector2 previousTarget = new Vector2(0, 0);
    private Direction direction = Direction.RIGHT;

    public Hero(SceneLoader sceneLoader, TileMapController tileMapController) {
        this.sceneLoader = sceneLoader;
        this.tileMapController = tileMapController;
    }

    @Override
    public void init(Entity entity) {
        this.entity = entity;
        transformComponent = ComponentRetriever.get(entity, TransformComponent.class);
        dimensionsComponent = ComponentRetriever.get(entity, DimensionsComponent.class);
    }

    @Override
    public void act(float delta) {
        // Coordinates infront of the hero sprite
        Vector2 target = new Vector2();
        target.x = getX() + dimensionsComponent.width/2;
        target.y = getY() + dimensionsComponent.height/2;

        float dx = 0, dy = 0;
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            dx = velocity * delta * -1;
            transformComponent.rotation = 180f;
            direction = Direction.LEFT;
        } else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            dx = velocity * delta;
            transformComponent.rotation = 0f;
            direction = Direction.RIGHT;
        }else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            dy = velocity * delta * -1;
            transformComponent.rotation = 270.0f;
            direction = Direction.DOWN;
        } else if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            dy = velocity * delta;
            transformComponent.rotation = 90.0f;
            direction = Direction.UP;
        }

        // Move base target to the middle
        target.x -= dimensionsComponent.width;
        target.y -= dimensionsComponent.height;
        // Move target to the square in front of the hero
        target.x += dimensionsComponent.width * direction.x;
        target.y += dimensionsComponent.height * direction.y;

        tileMapController.removeHighlight(previousTarget);
        tileMapController.highlightTarget(target);
        previousTarget.set(target);

        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
            tileMapController.useHoe(target);
        }

        // TODO: Move to different function
        float raySize = dx;
        float centerX = (transformComponent.x + dimensionsComponent.width/2) * PhysicsBodyLoader.getScale();
        float centerY = (transformComponent.y + dimensionsComponent.height/2) * PhysicsBodyLoader.getScale();

        Vector2 rayFrom = new Vector2(centerX, centerY);
        Vector2 rayTo = new Vector2(centerX + raySize * PhysicsBodyLoader.getScale(), centerY);

        blocked = false;
        if(raySize > 0) {
            sceneLoader.world.rayCast(new RayCastCallback() {
                @Override
                public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
                    blocked = true;

                    return 0;
                }
            }, rayFrom, rayTo);
        }

        if(!blocked) {
            transformComponent.x += dx;
            transformComponent.y += dy;
        }
    }

    @Override
    public void dispose() {

    }

    public float getX() {
        return transformComponent.x;
    }

    public float getY() {
        return transformComponent.y;
    }

    public static class Torch implements IScript{

        private TransformComponent transformComponent;
        private Hero hero;

        public Torch(Hero hero){
            this.hero = hero;
        }

        @Override
        public void init(Entity entity) {
            transformComponent = ComponentRetriever.get(entity, TransformComponent.class);
        }

        @Override
        public void act(float delta) {
            transformComponent.x = hero.getX();
            transformComponent.y = hero.getY();
        }

        @Override
        public void dispose() {

        }
    }
}
